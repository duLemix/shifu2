#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "toml.h"

#define SHIFU2_VERSION "0.1.0"

#define FNAME_MAX 256 /* maximum config file name size */

static void
error(const char *msg, const char *msg1)
{
    fprintf(stderr, "\033[1;31mERROR\033[0m: %s%s\n", msg, msg1?msg1:"");
    exit(1);
}

static void
version()
{
    fprintf(stdout, "v%s\n", SHIFU2_VERSION);
    exit(0);
}

static void
help()
{
    fprintf(stdout,
            "Usage: \033[1;32mshifu2-parse\033[0m \033[0;33mFILE\033[0m \033[0;34m[OPTIONS] [-h | --help]\033[0m\n"
            "\n"
            "\033[0;36mshifu2-parse\033[0m is a command-line tool written in C for \033[0;36mshifu2\033[0m,\n"
            "the package manager. It reads from the \033[0;4mtoml config file of a\033[0m\n" 
            "\033[0;4mpackage\033[0m and outputs a single requested information about it,\n" 
            "suck as package name, source URL, installation prefix, a\n"
            "list of dependencies, if any, and other things.\n"
            "\n"
           );
    fprintf(stdout,
            " \033[0;34mOPTIONS\033[0m\n"
            "   -h, --help              print this menu and quit\n"
            "   -V, --shifu2-version    print shifu2-parse version\n"
            "                           and quit\n"
            "\n"
            "   -n, --name              package name\n"
            "   -v, --version           package version\n"
            "   -d, --description       package description\n"
            "   -u, --url               package source URL\n"
            "   -b, --bin               whether package is binary\n"
            "   -p, --prefix            prefix to where package will be\n"
            "                           installed\n"
            "\n"
            "   -D, --dependencies      list of package dependencies;\n"
            "                           will return nothing if empty;\n"
            "                           will unset all other flags if\n"
            "                           selected\n"
            "\n"
           );
    fprintf(stdout,
            "Report bugs to \033[0;4mhttps://codeberg.org/duLemix/shifu2/issues\033[0m\n"
            "\n"
           );

    exit (0);
}

enum
info_t
{
    NAME,
    VERSION,
    DESCRIPTION,
    URL,
    BIN,
    PREFIX,
};

void
pkg_info(toml_table_t *package_info, int request)
{
    toml_datum_t info;
    switch (request)
    {
        case NAME:
            info = toml_string_in(package_info, "name");
            if (!info.ok)
                error("cannot read package name", "");
            break;
        case VERSION:
            info = toml_string_in(package_info, "version");
            if (!info.ok)
                error("cannot read package version", "");
            break;
        case DESCRIPTION:
            info = toml_string_in(package_info, "description");
            if (!info.ok)
                error("cannot read package description", "");
            break;
        case URL:
            info = toml_string_in(package_info, "url");
            if (!info.ok)
                error("cannot read package source URL", "");
            break;
        case BIN:
            info = toml_string_in(package_info, "bin");
            if (!info.ok)
                error("cannot read whether package is binary", "");
            break;
        case PREFIX:
            info = toml_string_in(package_info, "prefix");
            if (!info.ok)
                error("cannot read package installation prefix", "");
            break;
        default:
            error("invalid package information token", "");
            break;
    }

    fprintf(stdout, "%s\n", info.u.s);
}

void
pkg_deps(toml_table_t *package_deps)
{
    toml_array_t *deps = toml_array_in(package_deps, "deps");
    if (!deps)
        error("cannot read package dependencies", "");

    int d = 0, sz = toml_array_nelem(deps);
    toml_datum_t name, version;

    while (d < sz)
    {
        name = toml_string_at( toml_array_at(deps, d), NAME);
        if (!name.ok)
            error("cannot access dependency name", "");
        version = toml_string_at( toml_array_at(deps, d), VERSION);
        if (!version.ok)
            error("cannot access dependency version", "");

        ++d;
        fprintf(stdout, "%s:%s\n", name.u.s, version.u.s);
    }
}

int
main(int argc, char *argv[])
{
    int request = -1, D = 0;

    char errbuf[256];

    int opt;
    opterr = 0;

    const char *s_opts = "hVDnvdubp";
    const struct option l_opts[] =
    {
        { "help",           no_argument,    NULL,   'h' },
        { "shifu2-version", no_argument,    NULL,   'V' },
        { "dependencies",   no_argument,    NULL,   'D' },
        { "name",           no_argument,    NULL,   'n' },
        { "version",        no_argument,    NULL,   'v' },
        { "description",    no_argument,    NULL,   'd' },
        { "url",            no_argument,    NULL,   'u' },
        { "bin",            no_argument,    NULL,   'b' },
        { "prefix",         no_argument,    NULL,   'p' },
        { NULL, 0, NULL, 0 },
    };

    while ((opt = getopt_long(argc, argv, s_opts, l_opts, NULL)) != -1)
        switch (opt)
        {
            case 'h':
                help();
                break;
            case 'V':
                version();
                break;
            case 'D':
                D = 1;
                break;
            case 'n':
                request = NAME;
                break;
            case 'v':
                request = VERSION;
                break;
            case 'd':
                request = DESCRIPTION;
                break;
            case 'u':
                request = URL;
                break;
            case 'b':
                request = BIN;
                break;
            case 'p':
                request = PREFIX;
                break;
            case '?':
                error("invalid option or missing file\n", "Run `shifu2-parse -h` for help");
                break;
            default:
                error("unknown option", "");
                break;
        }

    if (D) /* if `-D` request is passed, ignore all others */
        request = -1;

    if (optind == argc)
        error("no config file provided\n", "Usage: \033[1;32mshifu2-parse\033[0m \033[0;33mFILE\033[0m \033[0;34m[OPTIONS] [-h | --help]\033[0m");

    /* first non-option argument is taken as input file,
     * don't fight me over that */
    FILE *fp = fopen(argv[optind], "r");
    if (!fp)
        error("cannot open file - ", strerror(errno));

    toml_table_t *conf = toml_parse_file(fp, errbuf, sizeof(errbuf));
    if (!conf)
        error("cannot parse - ", errbuf);

    fclose(fp);

    toml_table_t *p_info = toml_table_in(conf, "package");
    if (!p_info)
        error("missing [package] - ", "verify if the config file is complete");
    toml_table_t *p_deps = toml_table_in(conf, "dependencies");
    if (!p_deps)
        error("missing [dependencies] - ", "verify if the config file is complete");

    if (request > -1)
        pkg_info(p_info, request);
    if (D)
        pkg_deps(p_deps);

    return 0;
}
