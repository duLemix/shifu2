CC=zig cc

build: parse.o toml.o
	${CC} -o shifu2-parse	parse.o toml.o

parse.o:
	${CC} -o parse.o	-c src/parse.c

toml.o:
	${CC} -o toml.o	-c src/toml.c

clean:
	rm shifu2-parse
	rm parse.o toml.o
