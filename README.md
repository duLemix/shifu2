# Shifu2

**Shifu 2** The Package Manager, inspired on and spiritual successor o  [Shifu The Package Manager](https://codeberg.org/CurtainOS/shifu).

For instructions on usage and configuration, please refer to the [wiki](link_to_wiki) (you might have noticed that there is no wiki yet).

## Building
Change the compiler set in the `Makefile` file and run the following command on a terminal to compile:
```sh
make
```

## Installing
You shouldn't be installing this at this point

## Uninstalling
Why did you- HOW DID YOU INSTALL IT?

## FAQ

### Why is it empty?
I don't really know. Why are you empty in the soul?

### Why are you writing it in Bash/C?
As of now, this is just prototyping of a final product.
The main goal is to be able to write in [Zig](https://ziglang.org/) because it has lots of nice features as a language that seem useful for developing a package manager like this.

### But why not Zig directly?
Yeah, there's a thing called *I haven't learned enough of it*.

### When will it be ready?
I'm a college student, things might make it hard for dealing with side projects.

### But when will [CurtainOS](https://t.me/Curtainos) release?
In the year of the Linux Desktop.

## Contributing

If you'd like to contribute, please refer to our [Telegram channel](https://t.me/Curtainos) or our [Matrix-Telegram bridge channel](https://matrix.to/#/#curtainos_chat:matrix.org) and ask any of the admins and current developers for more information.

## Roadmap

[ ] Start a Bash zombie copy of Shifu and its interface
